package com.samadhan.creativenet.samadhan;

class _Menu {
    private String _menu;
    private int noOfMenu;
    private int thumbnail;

    public _Menu(){

    }

    public _Menu(String _menu, int noOfMenu, int thumbnail)
    {
        this._menu = _menu;
        this.noOfMenu = noOfMenu;
        this.thumbnail = thumbnail;
    }

    public String get_menu() {
        return _menu;
    }

    public void set_menu(String _menu) {
        this._menu = _menu;
    }

    public int getNoOfMenu() {
        return noOfMenu;
    }

    public void setNoOfMenu(int noOfMenu) {
        this.noOfMenu = noOfMenu;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
