package com.samadhan.creativenet.samadhan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class UserComplaints extends AppCompatActivity {

    private ArrayList<Complains> complaintList;
    private ListView list;
    private LazyAdapter adapter;
    private ProgressDialog progressDialog;
    private TextView txtCmpMsg;
    private TextView txtCmpCat;
    private TextView txtCmpDes;
    private TextView txtCmpDate;
    private TextView txtCmpStatus;
    private TextView txtCmpAddress;
    LinearLayout alertLayout;
    private ImageView imgComplainImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_complaints);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Complaints");
        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Receiving Complaints");
        progressDialog.setMessage(getString(R.string.lbl_fetch_comp));
        progressDialog.setCancelable(false);
        list= findViewById(android.R.id.list);
        txtCmpMsg= findViewById(R.id.txtCmpMsg);

        try {
            complaintList=new ArrayList<>();
            new GetUserComplaints().execute();
           /* int i=1;
            Complains comp;
            while(i<=4){
                comp=new Complains("BBNCANT3345454544","Sewarage","Sewar Is Choccked","Pending");
                complaintList.add(comp);
                i++;
            }*/

        }catch (Exception ex){
            Log.e("Main",ex.toString());
            ex.printStackTrace();
        }
    }
    private void setList(){
        if(complaintList.size()>0) {
            adapter = new LazyAdapter(this, complaintList);
            list.setAdapter(adapter);
            // Click event for single list row
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        Complains cmp = complaintList.get(position);
                        AlertDialog.Builder complaintalert=new AlertDialog.Builder(UserComplaints.this);
                        View dialogView=getLayoutInflater().inflate(R.layout.complaints_details,null);
                        complaintalert.setView(dialogView);
                        txtCmpCat= dialogView.findViewById(R.id.txtComplaintCat);
                        txtCmpDes= dialogView.findViewById(R.id.txtComplaintDes);
                        txtCmpDate= dialogView.findViewById(R.id.txtComplaintDate);
                        imgComplainImage= dialogView.findViewById(R.id.imgComplaint);
                        txtCmpStatus= dialogView.findViewById(R.id.txtComplaintStatus);
                        txtCmpAddress= dialogView.findViewById(R.id.txtComplaintAddress);
                        txtCmpCat.setText(cmp.getIssueType());
                        txtCmpDes.setText(cmp.getIssue());
                        txtCmpDate.setText(cmp.getDate());
                        txtCmpStatus.setText(cmp.getStatus());
                        txtCmpAddress.setText(cmp.getAddress());
                        new ImageLoadTask(cmp.getImagePath(),imgComplainImage).execute();

                        complaintalert.setTitle("Complaint Details");
                        complaintalert.setNegativeButton("X", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        complaintalert.show();

                    }catch (Exception ex){
                        Log.e("Select",ex.toString());
                        ex.printStackTrace();
                    }
                }
            });
            txtCmpMsg.setVisibility(View.GONE);
        }
        else{
            txtCmpMsg.setText("Fetching Your Complaints");
        }
    }

    private class GetUserComplaints extends AsyncTask<Void,Void,Void>{
        private  void getComplaints() throws UnsupportedEncodingException{
            String mobile=Utility.readValueFromPref("MobileNo",getApplicationContext());
            String data = URLEncoder.encode("mobile", "UTF-8")
                    + "=" + URLEncoder.encode(mobile, "UTF-8");
            String text = "";
            BufferedReader reader=null;
            try {
                URL url = new URL("http://cbbabina.org.in/cbb/API/APIGetUserComplains.php");
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine
                        ()) != null) {
                    sb.append(line + "\n");
                }
                text = sb.toString();
                complaintList=new ArrayList<>();
                String rows[]=text.split("#");
                Log.e("Data",String.valueOf(rows.length));
                Complains comp;
                String []status={"","Pending","Processing","Resolved"};

                for(int i=0;i<rows.length-1;i++){
                    comp=new Complains();
                    String []vals=rows[i].split("@");
                    comp.setRefno(vals[0]);
                    comp.setIssueType(vals[1]);
                    comp.setIssue(vals[2]);
                    comp.setDate(vals[5]);
                    comp.setImagePath(vals[6]);
                    comp.setStatus(status[Integer.valueOf(vals[4])]);
                    comp.setAddress(vals[7]);
                    complaintList.add(comp);
                }
                int complaincount = rows.length;
            }
            catch(Exception ex) {
                Log.e("API",ex.toString());
                ex.printStackTrace();
            }
            finally {
                try {
                    reader.close();
                }
                catch(Exception ex) {

                }
            }
        }
        @Override
        protected void onPreExecute() {
            progressDialog.show();
            setList();
            super.onPreExecute();
        }



        @Override
        protected Void doInBackground(Void... voids) {
            try{
                    getComplaints();
            }catch (Exception ex){
                Log.e("API",ex.toString());
                ex.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            super.onPostExecute(aVoid);
            setList();
        }
    }
    private class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

        private String url;
        private ImageView imageView;

        ImageLoadTask(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imageView.setImageBitmap(result);
        }

    }
}

