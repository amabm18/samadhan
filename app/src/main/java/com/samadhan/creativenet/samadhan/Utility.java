package com.samadhan.creativenet.samadhan;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

class Utility {
    public static boolean checkPermission(Activity paramActivity, String paramString) {
        return ContextCompat.checkSelfPermission(paramActivity, paramString) == 0;
    }

    public static String getLocationName(Location paramLocation, Context paramContext) {
        try {
            Geocoder paramGeoCoder = new Geocoder(paramContext);
            List<Address> myList = paramGeoCoder.getFromLocation(paramLocation.getLatitude(), paramLocation.getLongitude(), 1);
            StringBuilder paramContextBuilder = new StringBuilder();
            paramContextBuilder.append((myList.get(0)).getLocality());
            paramContextBuilder.append(",");
            paramContextBuilder.append((myList.get(0)).getSubAdminArea());
            paramContextBuilder.append(",");
            paramContextBuilder.append((myList.get(0)).getAddressLine(0));
            return paramContextBuilder.toString();
        } catch (Exception paramLocation1) {
            Log.e("LocationNm", paramLocation.toString());
            paramLocation1.printStackTrace();
        }
        return "";
    }

    public static String readValueFromPref(String paramString, Context paramContext) {
        return PreferenceManager.getDefaultSharedPreferences(paramContext).getString(paramString, "NA");
    }

    public static void requestPermission(Activity paramActivity, int paramInt, String paramString) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(paramActivity, paramString)) {
            Toast.makeText(paramActivity, "With Your Permission Some Feature Will Not Work Properly", Toast.LENGTH_LONG).show();
            return;
        }
        ActivityCompat.requestPermissions(paramActivity, new String[]{paramString}, paramInt);
    }

    public static void saveValueToPref(String paramString1, String paramString2, Context paramContext) {
        PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString(paramString1, paramString2).commit();
    }

    public static void showToast(Context paramContext, String paramString, int paramInt) {
        int i = 1;
        if (paramInt == 1) {
            i = 0;
        }
        Toast.makeText(paramContext, paramString, i).show();
    }
}
