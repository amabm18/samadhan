package com.samadhan.creativenet.samadhan;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Pattern;

public class Login extends AppCompatActivity {
    private EditText txtMobileno;
    private Button btnLogin;
    private String mobileNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(!NetworkUtility.isInternetAvailable(getApplicationContext())){
            View parentLayout = findViewById(R.id.loginRoot);
            Snackbar.make(parentLayout, "You Are Not Connected To Internet", Snackbar.LENGTH_INDEFINITE)
                    .setAction("X", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    })
                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                    .show();
        }
        if(!Utility.checkPermission(this,"android.permission.CAMERA")){
            Utility.requestPermission(this,1,"android.permission.CAMERA");
        }
        getViews();
    }
    private void getViews(){
        txtMobileno = findViewById(R.id.mobileNo);
        txtMobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()!=10){
                    txtMobileno.setTextColor(Color.RED);
                }else{
                    txtMobileno.setTextColor(Color.GREEN);
                }
            }
        });
        btnLogin = findViewById(R.id.btnLogin);
    }
    public void LoginHere(View view) {
            mobileNo=txtMobileno.getText().toString();
            if (mobileNo != "") {
                if(Pattern.matches("[6-9]{1}[0-9]{9}", mobileNo)) {
                    try {
                            Intent otpVerify=new Intent(Login.this,VerificationActivity.class);
                            otpVerify.putExtra("INTENT_PHONENUMBER", mobileNo);
                            otpVerify.putExtra("INTENT_COUNTRY_CODE", "91");
                            startActivity(otpVerify);
                            finish();
                    }catch (Exception ex){
                                    Log.e("Thread",ex.toString());
                                    ex.printStackTrace();
                    }
                }
                else {
                    txtMobileno.setError("Invalid Mobile No");
                }
            }
    }
}


