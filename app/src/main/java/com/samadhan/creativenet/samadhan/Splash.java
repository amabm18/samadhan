package com.samadhan.creativenet.samadhan;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class Splash extends AppCompatActivity {
    private TextView txtInit;
    private TextView txtApp;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void updateViews(String lang){
        Context context=LocaleHelper.setLocale(this,lang);
        Resources resources=context.getResources();
    }
    private void setLanguage(){
        if(Utility.readValueFromPref("Language",getApplicationContext()).toString().equalsIgnoreCase("Hindi")) {
            updateViews("en");
            //Utility.showToast(getApplicationContext(),"App Language Changed To Hindi",1);
           // Utility.saveValueToPref("Language","English",getApplicationContext());

        }
        else{
            updateViews("hi");

        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        setLanguage();
        txtInit= findViewById(R.id.txtSlogan);
        txtApp= findViewById(R.id.txtAppName);
         if(!Utility.checkPermission(Splash.this,"android.permission.ACCESS_COARSE_LOCATION")){
            Utility.requestPermission(Splash.this,3,"android.permission.ACCESS_COARSE_LOCATION");
        }

        Thread background = new Thread(){
            public void run(){
                try{
                    sleep(5000);
                    if(Utility.readValueFromPref("MobileNo",getApplicationContext()).equals("NA")){
                        Intent i = new Intent(Splash.this,Login.class);
                        startActivity(i);
                        finish();
                    }
                    else{
                        Intent i = new Intent(Splash.this,DemoNavigation.class);
                        startActivity(i);
                        finish();
                    }

                }catch(Exception ex) {

                }
            }
        };
        background.start();
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


}
