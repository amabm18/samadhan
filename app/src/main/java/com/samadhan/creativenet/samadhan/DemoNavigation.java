package com.samadhan.creativenet.samadhan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class DemoNavigation extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private boolean isShow = false;
    private MenuAdapter adapter;
    private List<_Menu> menuList;
    private TextView userMob;
    private TextView infoLink;
    private String lang;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void updateViews(String language) {
        Context context = LocaleHelper.setLocale(this, language);
        Resources resources = context.getResources();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_demo_navigation);
            lang = Utility.readValueFromPref("Language", getApplicationContext());
            RecyclerView recyclerView = findViewById(R.id.recycler_view);
            menuList = new ArrayList<>();
            adapter = new MenuAdapter(this, menuList);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addItemDecoration(new DemoNavigation.GridSpacingItemDecoration(3, dpToPx(0), true));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View hView = navigationView.getHeaderView(0);
            userMob = hView.findViewById(R.id.userMob);
            infoLink = hView.findViewById(R.id.textView);
            infoLink.setLinksClickable(true);
            infoLink.setText(getString(R.string.appname));
            infoLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://cbbabina.org.in/cbb/Login.php"));
                    startActivity(webIntent);
                }
            });
            Linkify.addLinks(infoLink, Linkify.WEB_URLS);
            userMob.setText(Utility.readValueFromPref("MobileNo", getApplicationContext()));
            prepareMenus();
            initCollapsingToolbar();
            Glide.with(this).load(R.drawable.swtbrt).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initCollapsingToolbar() {
        try {
            final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
            AppBarLayout appBarLayout = findViewById(R.id.appbar);
            appBarLayout.setExpanded(true);
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

                int scrollRange = -1;

                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        collapsingToolbar.setTitle(getString(R.string.app_name));

                        isShow = true;
                    } else if (isShow) {
                        collapsingToolbar.setTitle(" ");
                        isShow = false;
                    }
                }
            });
        } catch (Exception ex) {
            Log.e("Sama", ex.toString());
            ex.printStackTrace();
        }
    }

    private void prepareMenus() {
        try {
            int[] covers = new int[]{R.drawable.garbage, R.drawable.watercomplain, R.drawable.unauthconst, R.drawable._toilet, R.drawable.waste, R.drawable.animal, R.drawable.light, R.drawable.polltion, R.drawable.admin};

            _Menu a = new _Menu(getString(R.string.lbl_garbage), 1, covers[0]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_water), 2, covers[1]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_unauthrize), 3, covers[2]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_toilet), 4, covers[3]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_sewarage), 5, covers[4]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_animal), 6, covers[5]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_light), 7, covers[6]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_air), 8, covers[7]);
            menuList.add(a);

            a = new _Menu(getString(R.string.lbl_general), 9, covers[8]);
            menuList.add(a);

            adapter.notifyDataSetChanged();
        } catch (Exception ex) {
            Log.e("Sama", ex.toString());
            ex.printStackTrace();
        }
    }

    public void showComplaints(View view) {
        Intent userComplaints = new Intent(DemoNavigation.this, UserComplaints.class);
        startActivity(userComplaints);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            try {
                int position = parent.getChildAdapterPosition(view); // item position
                int column = position % spanCount; // item column

                if (includeEdge) {
                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect.top = spacing;
                    }
                    outRect.bottom = spacing; // item bottom
                } else {
                    outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                    outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                    if (position >= spanCount) {
                        outRect.top = spacing; // item top
                    }
                }

            } catch (Exception ex) {
                Log.e("Sama", ex.toString());
                ex.printStackTrace();
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.demo_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, AppPreference.class));
            return true;
        }
        if (id == R.id.mnLang) {

            if (Utility.readValueFromPref("Language", getApplicationContext()).toString().equalsIgnoreCase("Hindi")) {
                updateViews("hi");
                Utility.showToast(getApplicationContext(), "App Language Changed To Hindi", 1);
                Utility.saveValueToPref("Language", "English", getApplicationContext());
                recreate();

            } else {
                updateViews("en");
                Utility.showToast(getApplicationContext(), "App Language Changed To English", 1);
                Utility.saveValueToPref("Language", "Hindi", getApplicationContext());
                recreate();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.mnPending) {
            Intent ucomplaint = new Intent(DemoNavigation.this, UserComplaints.class);
            startActivity(ucomplaint);
        } else if (id == R.id.mnReport) {
            sendReport("Report Issue", false);

        } else if (id == R.id.mnRateUs) {
//            Utility.showToast(getApplicationContext(), "Rate Us", 1);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.samadhan.creativenet.samadhan")));
        } else if (id == R.id.mnAbout) {
            sendReport("About Us", true);
        } else if (id == R.id.mnShare) {
            // Utility.showToast(getApplicationContext(),"Share App",1);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.samadhan.creativenet.samadhan .Thanks Borinov Consulting Pvt. Ltd.");
            startActivity(Intent.createChooser(share, "Share Text"));
        } else if (id == R.id.mnExit) {
            finish();
        } else if (id == R.id.mnLogout) {
            Utility.saveValueToPref("MobileNo", "NA", getApplicationContext());
            finish();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void sendReport(String title, boolean isAbout) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        if (!isAbout) {
            builder.setView(R.layout.alert_layout);
            builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utility.showToast(getApplicationContext(), "Your Bug Report Send", Toast.LENGTH_SHORT);
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).show();
        } else {
            WebView msg = new WebView(this);
            //String mycontent="<html><body style='text-align:justify'>Samadhan app is an initiative by Babina Cantonment Board.This app will allow resident of Babina Cantonment to easily post their complaint regarding cleanliness and grievance of their own colony/area this app can be used 24 X 7.";
            String mycontent = "<html><body style='text-align:justify'>" + getString(R.string.aboutus) + "<div style='text-ailgn:right;font-size:14px;font-weight:bold;color:#fa7f1f;margin-top:20px;' align='right'>Design And Developed By<br>Borinovv Consulting Pvt. Ltd.</div></body></html>";
            msg.loadData(mycontent, "text/html", "UTF-8");
            builder.setView(msg);
            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).show();
        }
    }
}
