package com.samadhan.creativenet.samadhan;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;

import java.lang.Thread;

import android.widget.EditText;
import android.widget.Toast;

import com.msg91.sendotp.library.internal.VerificationCodeListener;

import java.util.Random;

public class OTPVerification extends AppCompatActivity implements VerificationCodeListener {
    private EditText otp1;
    private EditText otp2;
    private EditText otp3;
    private EditText otp4;
    private String otpNo;
    private ProgressDialog progressDialog;
    private void initViews(){
        otp1= findViewById(R.id.txtOtp1);
        otp2= findViewById(R.id.txtOtp2);
        otp3= findViewById(R.id.txtOtp3);
        otp4= findViewById(R.id.txtOtp4);
        otp1.setInputType(InputType.TYPE_CLASS_NUMBER);
        otp2.setInputType(InputType.TYPE_CLASS_NUMBER);
        otp3.setInputType(InputType.TYPE_CLASS_NUMBER);
        otp4.setInputType(InputType.TYPE_CLASS_NUMBER);
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
               if(editable.length()==1){
                   otp1.clearFocus();
                   otp2.requestFocus();
                   otp2.setCursorVisible(true);
               }
            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==1){
                    otp2.clearFocus();
                    otp3.requestFocus();
                    otp3.setCursorVisible(true);
                }
            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==1){
                    otp3.clearFocus();
                    otp4.requestFocus();
                    otp4.setCursorVisible(true);
                }
            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==1){
                    String inputOtp=otp1.getText().toString()+otp2.getText().toString()+otp3.getText().toString()+otp4.getText().toString();
                    if(inputOtp.equals(otpNo)){
                        Intent mainIntent=new Intent(OTPVerification.this,MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"You Input Wrong OTP",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);
        initViews();
        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Waiting For OTP");
        progressDialog.setMessage("Please Wait While OTP Arrives");
        progressDialog.setCancelable(false);
        otpNo=String.valueOf(System.currentTimeMillis());
        otpNo=otpNo.substring(otpNo.length()-4);
        final int num=new Random().nextInt(7-3)+3;
        progressDialog.show();
        new MyAsyncTask().execute();


    }
    private void addNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.sms)
                        .setContentTitle("Babina Samadhan OTP")
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentText(otpNo+" is your OTP (One Time Password) To Complete your verification please input it to use this service." );

        Intent notificationIntent = new Intent(this, OTPVerification.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    @Override
    public void onVerificationCode(String var1, String var2) {

    }

    @Override
    public void onVerificationCodeError(Exception var1) {

    }

    private class MyAsyncTask extends android.os.AsyncTask<Void,Void,Void>{

        protected void onPreExecute(){
            progressDialog.show();
        }
        protected Void doInBackground(Void ...param){
            try{
                int num=new Random().nextInt(7-2)+2;
                  Thread.sleep(5000*num);
                 addNotification();

            }catch(Exception ex){
            }
            return null;
        }
        protected void onPostExecute(Void param){
            progressDialog.dismiss();
        }

    }

}
