package com.samadhan.creativenet.samadhan;

class Complains {
    private String refno;
    private String issueType;
    private String issue;
    private String status;
    private String imagePath;
    private String address;
    public Complains(){

    }
    public Complains(String r,String t,String i,String s,String imgPath,String a){
        refno=r;
        issueType=t;
        issue=i;
        status=s;
        imagePath=imgPath;
        address=a;
    }
    public void setAddress(String a){
        address=a;
    }
    public String getAddress(){
        return address;
    }
    public void setImagePath(String imgPath){
        imagePath=imgPath;
    }
    public String getImagePath(){
        return "http://cbbabina.org.in/cbb/SamadhanUser/"+imagePath;
    }
    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }
}
