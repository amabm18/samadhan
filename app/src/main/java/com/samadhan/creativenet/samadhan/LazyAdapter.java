package com.samadhan.creativenet.samadhan;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class LazyAdapter extends BaseAdapter {

    private ArrayList<Complains> data;
    private static LayoutInflater inflater=null;
    private ImageLoader imageLoader;

    public LazyAdapter(Activity a, ArrayList<Complains> d) {
        Activity activity = a;
        data=d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);

        TextView title = vi.findViewById(R.id.title); // title
        TextView artist = vi.findViewById(R.id.artist); // artist name
        TextView duration = vi.findViewById(R.id.duration); // duration
        TextView issue= vi.findViewById(R.id.issue);
        ImageView thumb_image= vi.findViewById(R.id.list_image); // thumb image

        Complains song = new Complains();
        song = data.get(position);

        // Setting all values in listview
        title.setText(song.getRefno());
        artist.setText(song.getIssueType());
        duration.setText(song.getStatus());
        issue.setText(song.getIssue());
        thumb_image.setImageResource(R.drawable.nroundlogo);
        return vi;
    }
}