package com.samadhan.creativenet.samadhan;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.util.Log;

public class AppPreference extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SwitchPreference notify;
    EditTextPreference mobileNo;
//    ListPreference language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            addPreferencesFromResource(R.xml.appsettings);
//            this.language = ((ListPreference) findPreference("Language"));
            this.notify = ((SwitchPreference) findPreference("Notification"));
//            this.language.setValue("English");
//            this.language.setSummary("Application Language Is English");
            this.notify.setChecked(true);
        } catch (Exception paramBundle) {
            Log.e("Preference", paramBundle.toString());
            paramBundle.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
    }
}
