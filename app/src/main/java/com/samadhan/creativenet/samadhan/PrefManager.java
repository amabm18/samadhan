package com.samadhan.creativenet.samadhan;

import android.content.Context;
import android.content.SharedPreferences;

class PrefManager {
    public static void saveValueToPref(String key, String val, Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,val);
        editor.commit();
    }
    public static String readValueFromPref(String key, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);
        return sharedPreferences.getString("key","0");
    }
}
