package com.samadhan.creativenet.samadhan;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import android.widget.ImageView;

import com.bumptech.glide.Glide;


public class MainActivity extends AppCompatActivity {
    private boolean isShow = false;
    private MenuAdapter adapter;
    private List<_Menu> menuList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!Utility.checkPermission(this, "android.permission.ACCESS_COARSE_LOCATION")) {
            Utility.requestPermission(this, 1, "android.permission.ACCESS_COARSE_LOCATION");
        }
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        menuList = new ArrayList<>();
        adapter = new MenuAdapter(this, menuList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        prepareMenus();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initCollapsingToolbar();

        try {
            // Glide.with(this).load(R.drawable.canttlogo).into((ImageView) findViewById(R.id.backdrop));
            Glide.with(this).load(R.drawable.bnslider).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Babina Cannt");
        collapsingToolbar.setExpandedTitleTextColor(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));

        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));

                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Adding few albums for testing
     */
    private void prepareMenus() {
        int[] covers = new int[]{
                R.drawable.garbage,
                R.drawable.watercomplain,
                R.drawable.unauthconst,
                R.drawable._toilet,
                R.drawable.waste,
                R.drawable.animal,
                R.drawable.light,
                R.drawable.polltion,
                R.drawable.admin};

        _Menu a = new _Menu("GARBAGE / MALBA", 1, covers[0]);
        menuList.add(a);

        a = new _Menu("WATER COMPLAINT", 2, covers[1]);
        menuList.add(a);

        a = new _Menu("UNAUTHORISED CONSTRUCTION", 3, covers[2]);
        menuList.add(a);

        a = new _Menu("PUBLIC TOILET", 4, covers[3]);
        menuList.add(a);

        a = new _Menu("SEWERAGE", 5, covers[4]);
        menuList.add(a);

        a = new _Menu("ANIMAL/DOG", 6, covers[5]);
        menuList.add(a);

        a = new _Menu("STREET LIGHT", 7, covers[6]);
        menuList.add(a);

        a = new _Menu("AIR POLLUTION", 8, covers[7]);
        menuList.add(a);

        a = new _Menu("GENERAL ADMINISTRATION", 9, covers[8]);
        menuList.add(a);

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, AppPreference.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
