package com.samadhan.creativenet.samadhan;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class CustomizedListView extends Activity {
    // All static variables
    static final String URL = "https://api.androidhive.info/music/music.xml";
    // XML node keys
    static final String KEY_SONG = "song"; // parent node
    static final String KEY_ID = "id";
    static final String KEY_TITLE = "title";
    static final String KEY_ARTIST = "artist";
    static final String KEY_DURATION = "duration";
    static final String KEY_THUMB_URL = "thumb_url";

    private ListView list;
    private LazyAdapter adapter;
    private ArrayList<Complains> complaintList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_complaints);

        try {
            complaintList=new ArrayList<>();
            int i=1;
            Complains comp;
            while(i<=4){
                comp=new Complains("BBNCANT3345454544","Sewarage","Sewar Is Choccked","Pending","","");
                complaintList.add(comp);
                i++;
            }

            //XMLParser parser = new XMLParser();
            //String xml = parser.getXmlFromUrl(URL); // getting XML from URL
            //Document doc = parser.getDomElement(xml); // getting DOM element

            //NodeList nl = doc.getElementsByTagName(KEY_SONG);
            // looping through all song nodes &lt;song&gt;
            /*for (int i = 0; i < nl.getLength(); i++) {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                org.w3c.dom.Element e = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    e = (org.w3c.dom.Element) nl.item(i);
                }
                // adding each child node to HashMap key =&gt; value
                map.put(KEY_ID, parser.getValue(e, KEY_ID));
                map.put(KEY_TITLE, parser.getValue(e, KEY_TITLE));
                map.put(KEY_ARTIST, parser.getValue(e, KEY_ARTIST));
                map.put(KEY_DURATION, parser.getValue(e, KEY_DURATION));
                map.put(KEY_THUMB_URL, parser.getValue(e, KEY_THUMB_URL));

                // adding HashList to ArrayList
                songsList.add(map);
            }*/

            list = findViewById(android.R.id.list);

            // Getting adapter by passing xml data ArrayList
            adapter = new LazyAdapter(this, complaintList);
            list.setAdapter(adapter);

            // Click event for single list row
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });
        }catch (Exception ex){
            Log.e("Main",ex.toString());
            ex.printStackTrace();
        }
    }
}
