package com.samadhan.creativenet.samadhan;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Detail extends AppCompatActivity implements View.OnClickListener {
    private Bitmap bitmap;
    private String pos;
    private String response;
    private ImageView imageView;
    private ImageView imgDone;
    private EditText txtCaption;
    private EditText txtLocation;
    private Button btnEdit;
    private ProgressDialog progressDialog;
    LinearLayout appTop;
    private TextView txtRes;
    private LocationManager locationManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void updateViews(String lang) {
        Context context = LocaleHelper.setLocale(this, lang);
        Resources resources = context.getResources();
    }

    private void setLanguage() {
        if (Utility.readValueFromPref("Language", getApplicationContext()).toString().equalsIgnoreCase("Hindi")) {
            updateViews("en");
            //Utility.showToast(getApplicationContext(),"App Language Changed To Hindi",1);
            // Utility.saveValueToPref("Language","English",getApplicationContext());

        } else {
            updateViews("hi");

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        getViews();
        try {

            Uri uri = getIntent().getData();
            if (uri != null) {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } else {
                bitmap = getIntent().getParcelableExtra("BitmapImage");
            }
            pos = getIntent().getStringExtra("position");
            imageView.setImageBitmap(bitmap);
            setLanguage();
            txtCaption.setText(getString(R.string.lbl_issuedes));
            txtCaption.setOnClickListener(this);
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (!checkLocationPermission()) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                Location lstLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lstLocation == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, networkLocationListener);
                } else {
                    txtLocation.setText(Utility.getLocationName(lstLocation, getApplicationContext()));
                }

            }
        } catch (SecurityException ex) {
            Log.e("Security", ex.toString());
            ex.printStackTrace();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    public boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onClick(View v) {
        txtCaption.setText("");
    }

    private LocationListener networkLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location == null) {
                txtLocation.setText("We Are Having Problem In Finding Your Current Location Please Input Manually");
            } else {
                locationManager.removeUpdates(this);
                txtLocation.setText(Utility.getLocationName(location, getApplicationContext()));
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.appmenu, menu);
            MenuItem mnOpt = menu.getItem(0);
            mnOpt.setTitle(pos);
        } catch (Exception ex) {
            Log.e("Menu", ex.toString());
            ex.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void getViews() {
        imageView = findViewById(R.id.imageview);
        txtCaption = findViewById(R.id.txtCaption);
        txtLocation = findViewById(R.id.txtLocation);
        btnEdit = findViewById(R.id.btnLoc);
        imgDone = findViewById(R.id.btnDone);
        txtRes = findViewById(R.id.txtSerRes);
        txtLocation.setEnabled(false);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading Issue");
        progressDialog.setMessage("Issue Is Being Uploaded Please Wait");
        progressDialog.setCancelable(false);

    }

    public void editLocation(View view) {
        txtLocation.setEnabled(true);
        txtLocation.requestFocus();
    }

    public void saveDetail(View view) {
        if (txtCaption.getText().length() == 0 || txtLocation.getText().length() == 0) {
            txtCaption.setError("Description/Location Are Mandatory");
        } else {
            new UploadIssueTask().execute();
        }
    }

    private void setResponse() {
        txtRes.setText(response);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Detail.this, DemoNavigation.class);
                startActivity(i);
                finish();
            }
        }, 2000);

    }

    public void uploadImage() {
        try {
            final ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("image", convertBitmapToString(bitmap)));
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://cbbabina.org.in/cbb/UploadMobileImage.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            String responseStr = EntityUtils.toString(response.getEntity());
            Log.e("Image", responseStr);
        } catch (Exception ex) {
            Log.e("Image", ex.toString());
            ex.printStackTrace();
        }
    }

    public String convertBitmapToString(Bitmap bmp) {
        if (bmp != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();
            return android.util.Base64.encodeToString(b, android.util.Base64.DEFAULT);
        } else {
            return "";
        }
    }

    public void sendSMSAlert(String number) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            String mobnumber;
            if (pos.equalsIgnoreCase("Garbage / Malba") || pos.equalsIgnoreCase("Public Toilet") || pos.equalsIgnoreCase("Sewerage") || pos.equalsIgnoreCase("Animal / Dog") || pos.equalsIgnoreCase("Air Pollution")) {
                mobnumber = "7309036885,7309036886";
                //mobnumber="8707411313,8969303857,9889725621";
            } else if (pos.equalsIgnoreCase("Water Complaint") || pos.equalsIgnoreCase("Street Light")) {
                mobnumber = "7309036884";
                // mobnumber="8707411313,8969303857,9889725621";
            } else {
                mobnumber = "9795086668";
                //mobnumber="8707411313,8969303857,9889725621";
            }
            Log.e("mobile", mobnumber);
            String url = "http://api.msg91.com/api/sendhttp.php?sender=BBNCNT&route=4&mobiles=" + mobnumber + "&authkey=214712ABK02eV8eEKd5af3d7bf&country=91&message=" + URLEncoder.encode("Dear Babina Cantonment Official You Have A New Complain For " + pos + " From " + txtLocation.getText().toString() + " Your Complain Is " + txtCaption.getText().toString() + "\n Kindly Fix The Issue As Soon As Possible");
            HttpGet sendSMSRequest = new HttpGet(url);
            HttpResponse response = httpclient.execute(sendSMSRequest);
            String responseStr = EntityUtils.toString(response.getEntity());
            Log.e("Response", responseStr);
        } catch (IOException ex) {
            Log.e("SMS", ex.toString());
            ex.printStackTrace();
        }

    }

    private void sendIssue() throws UnsupportedEncodingException {
        String issue = txtCaption.getText().toString();
        String address = txtLocation.getText().toString();
        String mobile = Utility.readValueFromPref("MobileNo", getApplicationContext());
//        String image = convertBitmapToString(bitmap);
        String data = URLEncoder.encode("issue", "UTF-8")
                + "=" + URLEncoder.encode(issue, "UTF-8");
        data += "&" + URLEncoder.encode("issuetype", "UTF-8")
                + "=" + URLEncoder.encode(pos, "UTF-8");

        data += "&" + URLEncoder.encode("address", "UTF-8") + "="
                + URLEncoder.encode(address, "UTF-8");

        data += "&" + URLEncoder.encode("mobile", "UTF-8")
                + "=" + URLEncoder.encode(mobile, "UTF-8");
        final ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("image", convertBitmapToString(bitmap)));
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://cbbabina.org.in/cbb/API/UploadMobileImage.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            String responseStr = EntityUtils.toString(response.getEntity());
            Log.e("response", responseStr);
        } catch (IOException iox) {
            Log.e("ImageUpl", iox.toString());
            iox.printStackTrace();
        }
        String text = "";
        BufferedReader reader = null;
        try {
            URL url = new URL("http://cbbabina.org.in/cbb/API/APIAddComplain.php");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            text = sb.toString();
        } catch (Exception ex) {
            Log.e("API", ex.toString());
            ex.printStackTrace();
        } finally {
            try {

                reader.close();
            } catch (Exception ignored) {
            }
        }
        response = text;
    }


    private class UploadIssueTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                sendIssue();
                sendSMSAlert("9889725621");
            } catch (Exception ex) {
                Log.e("BACK", ex.toString());
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            setResponse();
            View parentLayout = findViewById(R.id.rootIssue);
            Snackbar.make(parentLayout, "Your Issue/Image Successfully Uploaded", Snackbar.LENGTH_SHORT).show();
        }
    }
}
