package com.samadhan.creativenet.samadhan;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TakePhoto extends AppCompatActivity {
    Uri imgUri;
    private ImageView imageView;
    private Button btnClick;
    private Button btnGal;
    private String pos;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void updateViews(String lang){
        Context context=LocaleHelper.setLocale(this,lang);
        Resources resources=context.getResources();
    }
    private void setLanguage(){
        if(Utility.readValueFromPref("Language",getApplicationContext()).toString().equalsIgnoreCase("Hindi")) {
            updateViews("en");
        }
        else{
            updateViews("hi");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        if( !Utility.checkPermission(this,"android.permission.WRITE_EXTERNAL_STORAGE")){
            Utility.requestPermission(this,1,"android.permission.WRITE_EXTERNAL_STORAGE");
        }
        Bundle vals = getIntent().getExtras();
        pos =vals.getString("position");
        getViews();
        checkPermission();
        setLanguage();
        Animation shake;
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        btnClick.startAnimation(shake);
        btnGal.startAnimation(shake);
        ActionBar actionBar= getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.appmenu, menu);
            MenuItem mnOpt = menu.getItem(0);
            mnOpt.setTitle(pos);
        }catch (Exception ex){
            Log.e("Menu",ex.toString());
            ex.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void getViews(){
        imageView = findViewById(R.id.imageview);
        btnClick = findViewById(R.id.button_image);
        btnGal= findViewById(R.id.button_gallery);
    }
    private void checkPermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
        {
            btnClick.setEnabled(false);
            Utility.showToast(getApplicationContext(),"Without Your Permission Some Features Will Not Work",2);
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
        }
    }
    public void takePhoto(View view){
            try {
                Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //imgUri = Uri.fromFile(getOutputMediaFile());
                //photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                startActivityForResult(photoIntent, 11);
            }catch (Exception ex){
                Log.e("Camera",ex.toString());
                ex.toString();
            }
    }
    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "BabinaCannt");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==11){
            if(resultCode==RESULT_OK){
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(TakePhoto.this,Detail.class);
                intent.putExtra("BitmapImage",thumbnail);
                intent.putExtra("position",pos);
                startActivity(intent);
                finish();
               // imageView.setImageBitmap(thumbnail);


            }
        }
        if(requestCode==12){
            if(resultCode==RESULT_OK && data!=null){

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);

                    if (cursor == null || cursor.getCount() < 1) {
                        return; // no cursor or no record. DO YOUR ERROR HANDLING
                    }

                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                    if(columnIndex < 0) // no column index
                        return; // DO YOUR ERROR HANDLING

                    String picturePath = cursor.getString(columnIndex);

                    cursor.close(); // close cursor

                Bitmap thumbnail = BitmapFactory.decodeFile(picturePath);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Intent intent = new Intent(TakePhoto.this,Detail.class);
                intent.putExtra("position",pos);
                intent.setData(selectedImage);
                startActivity(intent);
                finish();
            }
        }
    }
    public void skipPhoto(View view) {
        Intent intent=new Intent(TakePhoto.this,Detail.class);
        intent.putExtra("from","no");
        intent.putExtra("position",pos);
        startActivity(intent);
        finish();
    }

    public void shakeImage(View view) {
        Animation shake;
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        btnClick.startAnimation(shake);
        btnGal.startAnimation(shake);
    }

    public void fromGallery(View view) {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            //intent.setType("image/*");
            //intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, 12);
        }catch (Exception ex)
        {

        }
    }
}

